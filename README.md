php 数据类型检查 与格式化 library (require php >=7.0)
====================

> 1.支持基本类型检查与格式化 
   
   基本类型:   
   String,Number(Integer,Float[Double]),  
   Array(array,NumberArr,ListMap,HashMap),   
   Object,Null,Boolean,Resource,Closure,Callable       
   
   包括类型别名+缩写支持   
   json化后类型检查 与格式化还原  
   serialize 化类型检查 与格式化还原   
   
    eg :  
        0 === Number::format("0")
        null === Nil::format("N;")
        true == Nil::isSerailize("N;")  
        
        false === Number::verify(null)   
        true == NumberArr::verify([0,1,2,3])
        false == NumberArr::verify([0,'test'=>1,'php'])
   
> 2.支持反射检查 Any 工具类

    eg :
     $bool = Any::isInstance(Type:class,New Number)
     $interfaces = Any::hasInterfaces(Number::class)
      
     $result = Any::hasInterfaces(Number::class,[Type::class])
     if($result[Type::class]){
         echo ""has interface Type";
     }
    