<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 11:05
 */

namespace WebLinuxGame\DateType\Contracts;

/**
 * Interface Type
 * @package WebLinuxGame\BaseType\Contracts
 */
interface Type
{

    /**
     * 格式化
     * @param $data
     * @param null $default
     * @return mixed
     */
    public static function format($data,$default = null);

    /**
     * 验证
     * @param $data
     * @return bool
     */
    public static function verify($data) : bool ;

    /**
     * 输出类型名
     */
    public static function type(): string;

    /**
     * 是否对应类型名
     * @param string $name
     * @return bool
     * @throws \Exception
     */
    public static function isName(string $name): bool;

    /**
     * 是否别名
     * @param string $name
     * @return bool
     */
    public static function isAlias(string $name): bool;

    /**
     * 是否对应类型code
     * @param int $code
     * @return bool
     */
    public static function isType(int $code): bool;
}