<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 11:23
 */

namespace WebLinuxGame\DateType\Traits;

use Exception;
use ReflectionClass;

/**
 * 反射常用
 * Trait ReflectionClassTrait
 * @package core\traits
 */
trait ReflectionTrait
{
    use LoggerTrait;

    /**
     * 是否子类或子实例
     * @param string|object $class
     * @param string $parent
     * @return bool
     */
    public static function isInstanceOf($class, string $parent)
    {
        if (!class_exists($parent)) {
            return false;
        }
        if (is_object($class) && $class instanceof $parent) {
            return true;
        }
        if (!is_string($class)) {
            return false;
        }
        if (!class_exists($class)) {
            return false;
        }
        if ($class == $parent) {
            return true;
        }
        $oReflection = static::getReflection($class);
        if (empty($oReflection)) {
            return false;
        }
        if ($oReflection->isSubclassOf($parent)) {
            return true;
        }

        return false;
    }

    /**
     * 获取类实现的接口|判断类是否实现了某些接口
     * @param string|object $class
     * @param array $interfaces
     * @return array|bool
     */
    public static function hasInterface($class, array $interfaces = [])
    {
        if (is_object($class)) {
            $class = get_class($class);
        }
        if (!is_string($class)) {
            return false;
        }
        $oReflection = static::getReflection($class);
        if (empty($oReflection)) {
            return false;
        }
        if (empty($interfaces)) {
            return $oReflection->getInterfaceNames();
        }
        $arrRet = [];
        foreach ($interfaces as $interface) {
            if (empty($interface)) {
                $arrRet[$interface] = false;
                continue;
            }
            if ($oReflection->implementsInterface($interface)) {
                $arrRet[$interface] = true;
                continue;
            }
            $arrRet[$interface] = false;
        }
        return $arrRet;
    }

    /**
     * 是含有对应功能块
     * @param $class
     * @param array $traits
     * @return array|bool|ReflectionClass[]
     */
    public static function hasTraits($class,array $traits = [])
    {
        if (is_object($class)) {
            $class = get_class($class);
        }
        if (!is_string($class)) {
            return false;
        }
        $oReflection = static::getReflection($class);
        if (empty($oReflection)) {
            return false;
        }
        if (empty($traits)) {
            return $oReflection->getTraits();
        }
        $arrRet = [];
        $traitArr = $oReflection->getTraits();
        if(empty($traitArr)){
            return false;
        }
        foreach ($traits as $trait) {
            if (empty($traitArr[$trait])) {
                $arrRet[$trait] = false;
                continue;
            }
            $arrRet[$trait] = true;
        }
        return $arrRet;
    }

    /**
     * 获取类定义的常量|判断类是否了定义的某些常量
     * @param string|object $class
     * @param array $const
     * @return array|bool
     */
    public static function hasConst($class, array $const = [])
    {
        if (is_object($class)) {
            $class = get_class($class);
        }
        if (!is_string($class)) {
            return [];
        }
        $oReflection = static::getReflection($class);
        if (empty($oReflection)) {
            return false;
        }
        if (empty($const)) {
            return $oReflection->getConstants();
        }
        $arrRet = [];
        foreach ($const as $it) {
            if (empty($it) || !is_string($it)) {
                return $arrRet;
            }
            if ($oReflection->hasConstant($it)) {
                $arrRet[$it] = true;
                continue;
            }
            $arrRet[$it] = false;
        }
        return $arrRet;
    }

    /**
     * 是否某类的实例
     * @param string|object $class
     * @param $oInstance
     * @return array|bool
     */
    public static function isInstance($class, $oInstance)
    {
        if (empty($oInstance) || !is_object($oInstance)) {
            return false;
        }
        if (is_object($class)) {
            if ($class == $oInstance) {
                return true;
            }
            $class = get_class($class);
        }
        if (!is_string($class)) {
            return false;
        }
        $oReflection = static::getReflection($class);
        if (empty($oReflection)) {
            return false;
        }
        return $oReflection->isInstance($oInstance);
    }

    /**
     * 获取某类反射对象
     * @param string|object $class
     * @return ReflectionClass|null
     */
    public static function getReflection($class)
    {
        if (is_object($class)) {
            $class = get_class($class);
        }
        if (!is_string($class)) {
            return null;
        }
        try {
            $oReflection = new ReflectionClass($class);
            return $oReflection;
        } catch (Exception $e) {
            $log = [
                'msg' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'args' => compact('class'),
            ];
            $file = getenv('error_log_file');
            if (empty($file) || !is_string($file)) {
                $file = 'error.log';
            }
            self::write('#relection_error#', $log, $file);
        }
        return null;
    }
}