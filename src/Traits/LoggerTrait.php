<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 11:26
 */

namespace WebLinuxGame\DateType\Traits;

/**
 * Trait LoggerTrait
 * @package WebLinuxGame\DateType\Traits
 */
trait LoggerTrait
{
    /**
     * @param string $log
     * @param array $context
     * @param string $file
     * @param int $type
     * @return bool
     */
    public static function write(string $log, array $context = [], $file = 'error.log', int $type = 3)
    {
        $template = '[%s] %s %s';
        $level = preg_match('/err/', $file) ? 'ERROR' : 'INFO';
        if (!empty($context)) {
            $log = $log .
                PHP_EOL .
                json_encode($context, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT) .
                PHP_EOL;
        }
        $msg = sprintf($template, date('Y-m-d H:i:s'), $level, $log);
        return error_log($msg, $type, $file);
    }
}