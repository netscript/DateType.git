<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/8
 * Time: 10:20
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Abstracts\BaseType;

/**
 * 字符串类型
 * Class Str
 * @package Main\Api\DataType
 */
class Boolean extends BaseType
{
    const TYPE_CODE = 0x00002;

    protected static $type = 'boolean';

    protected static $alias = ['bool', 'b',];

    /**
     * 格式化
     * @param $data
     * @param Nil $default
     * @return string
     */
    public static function format($data, $default = null)
    {
        if (self::verify($data)) {
            return (bool)$data;
        }
        if (is_string($data)) {
            if(self::isJsonBool($data)){
                return (bool)json_decode($data);
            }
            if(self::isSerializeBool($data)){
                return (bool)unserialize($data);
            }
        }
        if(!empty($data)){
            return true;
        }
        return (bool)$default;
    }

    /**
     * 验证类型
     * @param $data
     * @return bool
     */
    public static function verify($data): bool
    {
        if (is_bool($data)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为json 格式字符串
     * @param string $data
     * @return bool
     */
    public static function isJsonBool(string $data): bool
    {
        if (preg_match('/^(true|false)$/', $data)) {
            return true;
        }
        return false;
    }

    /**
     * 是否为序列化字符串
     * @param string $data
     * @return bool
     */
    public static function isSerializeBool(string $data): bool
    {
        if (preg_match('/^b:(0|1);$/', $data)) {
            return true;
        }
        return false;
    }

}