<?php
/**
 * Created by PhpStorm.
 * User: WebLinuxGame
 * Date: 2019/6/9
 * Time: 11:23
 */

namespace WebLinuxGame\DateType\Support\Types;

use WebLinuxGame\DateType\Traits\ReflectionTrait;

/**
 * Class Any
 * @package WebLinuxGame\DateType\Support\Types
 */
class Any
{
   use ReflectionTrait;
}